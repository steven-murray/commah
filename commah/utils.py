'''
Utility functions for commah 
'''

#===============================================================================
# Imports
#===============================================================================
import numpy as np

#===============================================================================
# Routines
#===============================================================================
# This izip routine is from itertools, saves user having to download extra
# package for this short routine
# def izip(*iterables):
#   # izip('ABCD', 'xy') --> Ax By
#   iterators = map(iter, iterables)
#   while iterators:
#     yield tuple(map(next, iterators))

def check_input(zi, Mi, z=None, verbose=None):
  """ Ensure user input be it scalar, array or numpy array don't break the code """

  # Convert given redshifts to np.array
  zi = np.array([zi]).flatten()
  lenz = len(zi)

  # Convert given masses to np.array
  Mi = np.array([Mi]).flatten()
  lenm = len(Mi)

  # # Check the input sizes for zi and Mi make sense, if not then exit unless
  # # one axis is length one, in which case replicate values to the size of the other
  if (lenz > 1) and (lenm > 1):
    if lenz != lenm:
      raise ValueError("Ambiguous request: Need individual redshifts for all haloes provided or all haloes at same redshift")

  elif (lenz == 1) and (lenm > 1):
    if verbose:
      print "Assuming zi is the same for all Mi halo masses provided"
    # # Replicate redshift for all halo masses
    zi = np.repeat(zi, lenm)
    lenz = len(zi)
  elif (lenm == 1) and (lenz > 1):
    if verbose:
      print "Assuming Mi halo masses are the same for all zi provided"
    Mi = np.repeat(Mi, lenz)
    lenm = len(Mi)

  # # Complex test for size / type of incoming array, just in case numpy / list given
  if z is None:
    # if not given, set z to zi
    z = zi
  else:
    z = np.array([zi]).flatten()

  lenzout = len(z)

  return zi, Mi, z, lenz, lenm, lenzout
